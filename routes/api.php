<?php

use App\Http\Controllers\ReferralController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/create', [ReferralController::class, 'store']);
Route::put('/update/{id}', [ReferralController::class, 'update']);
Route::post('/web', [ReferralController::class, 'calculationPayments'])->name('calculationPayments');
Route::get('/get-user-data/{id}', [ReferralController::class, 'get']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
