<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6 mt-5 order-md-1">

            <form class="form-group row" method="post" action="{{route('calculationPayments')}}">

                <div class="mb-3">
                    <label for="inputAddress" class="form-label">Количество аллокаций</label>
                    <input type="text" required name="number_allocations" class="form-control" id="inputAddress" placeholder="Количество аллокаций" >
                </div>
                <div class="mb-3">
                    <label for="inputAddress2" class="form-label">Стоимость одной аллокации</label>
                    <input type="text" required name="amount" class="form-control" id="inputAddress2" placeholder="Стоимость одной аллокации">
                </div>
                <div class="mb-3">
                    <label for="inputAddress2" class="form-label">Указание процента прибыли</label>
                    <input type="text" required name="profit_percentage" class="form-control" id="inputAddress3" placeholder="Указание процента прибыли">
                </div>
                <button type="submit" class="btn btn-primary">Send</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
