<?php

namespace App\Console\Commands;

use App\Models\Members;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class RecountReferral extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:recount-referral';

    protected $countRef;
    protected $data = [];
    protected $levelCount = 0;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'RecountReferral every night';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     */
    public function handle(): void
    {
        $users = Members::all();

        foreach ($users as $user) {
            $countRef = $this->recountRef($user);
            $this->countRef = 0;

            $this->data[] = [$user->login, $user->login_referral, $user->full_name, date("d.m.y h:i:s"), $this->levelCount];

            $refUsers = Members::query()
                ->where(Members::FIELD_LOGIN_REFERRAL, $user->login)->get();

            foreach ($refUsers as $refUser) {
                $this->levelCount++;
                $this->data[] = [$refUser->login, $refUser->login_referral, $refUser->full_name, date("d.m.y h:i:s"), $this->levelCount];
                $this->getTreeForUser($refUser);
                $this->levelCount = 0;
            }

            $user->referral_tree = $this->writeFile($this->data);

            $user->count_subscribers = $countRef;

            $user->save();

            $this->data = [];
        }

        exit();
    }

    /**
     * @param Members $user
     * @param int $countRef
     */
    public function recountRef(Members $user): int
    {
        $refUsers = Members::query()
            ->where(Members::FIELD_LOGIN_REFERRAL, $user->login);

        if ($refUsers->count() > 0) {
            $users = $refUsers->get();
            $this->countRef += $refUsers->count();

            foreach ($users as $user) {
                $this->recountRef($user);
            }
        }

        return (int) $this->countRef;
    }

    private function  getTreeForUser(Members $user): array
    {
        $refUsers = Members::query()
            ->where(Members::FIELD_LOGIN_REFERRAL, $user->login);

        if ($refUsers->count() != 0) {
            $users = $refUsers->get();

            $this->levelCount++;

            foreach ($users as $user) {
                $this->data[] = [$user->login, $user->login_referral, $user->full_name, date("d.m.y h:i:s"), $this->levelCount];
                $this->getTreeForUser($user);
            }
            $this->levelCount -= $users->count();
        }

        return $this->data;
    }

    /**
     * @param array $data
     * @return string
     */
    function writeFile(array $data): string
    {
        $current_timestamp = Carbon::now()->timestamp;

        $uniName = uniqid(rand(), true);

        $relativePath = "recount-referral/export-$uniName-$current_timestamp.xlsx";


        $fullFilePath = storage_path() . "/app/public/" . $relativePath;

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // Структура документа
        $sheet->setCellValue('A1', 'login');
        $sheet->setCellValue('B1', 'login_referral');
        $sheet->setCellValue('C1', 'full_name');
        $sheet->setCellValue('D1', 'date');
        $sheet->setCellValue('E1', 'levelCount');

        $i = 2;
        foreach ($data as $row) {
            $login = $row[0]; //login
            $login_referral = $row[1]; //login_referral
            $full_name = $row[2]; //full_name
            $date = $row[3]; //date
            $levelCount = $row[4]; //levelCount

            $sheet->setCellValue('A' . $i, $login);
            $sheet->setCellValue('B' . $i, $login_referral);
            $sheet->setCellValue('C' . $i, $full_name);
            $sheet->setCellValue('D' . $i, $date);
            $sheet->setCellValue('E' . $i, $levelCount);

            $i++;
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($fullFilePath);

        return $relativePath;
    }
}
