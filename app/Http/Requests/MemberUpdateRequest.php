<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'external_id' => 'string|max:255',
            'login' => 'string|max:255',
            'login_referral' => 'string|max:255',
            'count_subscribers' => 'integer',
            'referral_tree' => 'string|max:255',
            'balance' => 'double',
            'wallet_number' => 'string',
            'full_name' => 'string|max:255'
        ];
    }
}
