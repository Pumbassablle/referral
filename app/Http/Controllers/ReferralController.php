<?php

namespace App\Http\Controllers;

use App\Console\Commands\RecountReferral;
use App\Http\Requests\CalculationPayments;
use App\Http\Requests\MemberRequest;
use App\Http\Requests\MemberUpdateRequest;
use App\Models\Members;
use Illuminate\Http\JsonResponse;

class ReferralController extends Controller
{

    public function index()
    {
        return view('web');
    }

    /**
     * @param MemberRequest $request
     * @return JsonResponse
     */
    public function store(MemberRequest $request): JsonResponse
    {
        $validated = $request->validated();
        $status = 'error';

        $member = Members::create($validated);

        if ($member->save($request->all())) {
            $status = 'ok';
        }

        return response()->json(['status' => $status]);
    }

    public function update(MemberUpdateRequest $request, $external_id): JsonResponse
    {
        $member = Members::where('external_id', $external_id)->firstOrFail();
        $status = 'error';

        if ($member->update($request->all())) {
            $status = 'ok';
        }

        return response()->json([
            'status' => $status,
        ]);
    }

    public function get($external_id): JsonResponse
    {
        $member = Members::where('external_id', $external_id)->firstOrFail();

        return response()->json([
            'status' => 'ok',
            'path' => asset("storage/" . $member->referral_tree),
            'count_subscribers' => $member->count_subscribers,
            'balance' => $member->balance,
            'full_name' => $member->full_name
        ]);
    }

    public function calculationPayments(CalculationPayments $request)
    {
        $validated = $request->validated();

        $users = Members::all();
        $countUsers = $users->count();

        $countLeadersLv1 = 0;
        $countLeadersLv2 = 0;
        $countLeadersLv3 = 0;
        $countLeadersLv4 = 0;
        $countLeadersLv5 = 0;

        foreach ($users as $user) {
            if ($user->count_subscribers >= 50) {
                $countLeadersLv1++;
            }
            if ($user->count_subscribers >= 100) {
                $countLeadersLv2++;
            }
            if ($user->count_subscribers >= 500) {
                $countLeadersLv3++;
            }
            if ($user->count_subscribers >= 1000) {
                $countLeadersLv4++;
            }
            if ($user->count_subscribers >= 3000) {
                $countLeadersLv5++;
            }
        }

        $number_allocations = (float)$validated['number_allocations'];
        $profit_percentage = (float)$validated['profit_percentage'];
        $amount = (float)$validated['amount'];

        foreach ($users as $user) {

            //V = Y*Z
            $allocationsVolume = $number_allocations * $amount;

            //R = V*W
            $income = $allocationsVolume * $profit_percentage / 100;

            //M = R-V
            $netProfit = $income - $allocationsVolume;

            //N = M * 7%
            $amountRemunerationDistributed = $netProfit * 0.07;

            //A = N / X
            $personalRewardEachUser = $amountRemunerationDistributed / $countUsers;

            $amountDistributedLeadershipRemuneration   = $netProfit * 0.01;

            $q = 0;

            if ($user->count_subscribers >= 50) {
                $q += $amountDistributedLeadershipRemuneration / $countLeadersLv1;
            }
            if ($user->count_subscribers >= 100) {
                $q += $amountDistributedLeadershipRemuneration / $countLeadersLv2;
            }
            if ($user->count_subscribers >= 500) {
                $q += $amountDistributedLeadershipRemuneration / $countLeadersLv3;
            }
            if ($user->count_subscribers >= 1000) {
                $q += $amountDistributedLeadershipRemuneration / $countLeadersLv4;
            }
            if ($user->count_subscribers >= 3000) {
                $q += $amountDistributedLeadershipRemuneration / $countLeadersLv5;
            }

            $itog = $personalRewardEachUser + $q;
            $user->balance = $itog;
            $user->save();
        }

        return response()->json([
            'status' => 'ok',
        ]);
    }
}
