<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $ext_id
 * @property string $login
 * @property string $login_referral
 * @property int $count_subscribers
 * @property string $path
 */
class RecountReferral extends Model
{
    use HasFactory;

    protected $table = 'recount_referral';

    const FIELD_LOGIN = 'login';
    const FIELD_LOGIN_REFERRAL = 'login_referral';
    const FIELD_COUNT_SUBSCRIBERS = 'count_subscribers';
    const FIELD_REFERRAL_TREE = 'referral_tree';
    const FIELD_BALANCE = 'balance';

    protected $fillable = [
        'ext_id',
        'login',
        'login_referral',
        'count_subscribers',
    ];
}
