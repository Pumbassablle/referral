<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property string $external_id
 * @property string $login
 * @property string $login_referral
 * @property int $count_subscribers
 * @property string $referral_tree
 * @property float $balance
 * @property string $full_name
 */
class Members extends Model
{
    use HasFactory;

    protected $table = 'members';

    const FIELD_LOGIN = 'login';
    const FIELD_LOGIN_REFERRAL = 'login_referral';
    const FIELD_COUNT_SUBSCRIBERS = 'count_subscribers';
    const FIELD_REFERRAL_TREE = 'referral_tree';
    const FIELD_BALANCE = 'balance';

    protected $fillable = [
        'external_id',
        'login',
        'login_referral',
        'count_subscribers',
        'referral_tree',
        'balance',
        'wallet_number',
        'full_name',
        'payment_amount',
        'payment_status'
    ];

}
